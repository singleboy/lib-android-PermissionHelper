package com.bookbuf.android.permission;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by robert on 16/5/28.
 */
public class IncreaseHelper {

	private final AtomicInteger sNextGeneratedRequestCode = new AtomicInteger (1);

	/*分配唯一id*/
	public int obtainId () {
		for (; ; ) {
			final int result = sNextGeneratedRequestCode.get ();
			int newValue = result + 1;
			if (newValue > 0x00FFFFFF) {
				newValue = 1; // Roll over to 1, not 0.
			}
			if (sNextGeneratedRequestCode.compareAndSet (result, newValue)) {
				return result;
			}
		}
	}

}
