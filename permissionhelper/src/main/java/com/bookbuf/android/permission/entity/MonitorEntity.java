package com.bookbuf.android.permission.entity;

import com.bookbuf.android.permission.inf.OnRequestPermissionsResultCallback;

/**
 * Created by robert on 16/5/27.
 */
public abstract class MonitorEntity<Container> {

	public MonitorEntity (Container container, OnRequestPermissionsResultCallback<?> callback) {
		this.container = container;
		this.callback = callback;
	}

	private OnRequestPermissionsResultCallback<?> callback;

	private Container container;

	/*仅作唯一标识符*/
	private int id = -1;

	public int getId () {
		return id;
	}

	public void setId (int id) {
		this.id = id;
	}

	public boolean isIdValid () {
		return id != -1;
	}

	public OnRequestPermissionsResultCallback<?> getCallback () {
		return callback;
	}

	public void setCallback (OnRequestPermissionsResultCallback<?> callback) {
		this.callback = callback;
	}

	public Container getContainer () {
		return container;
	}

	public void setContainer (Container container) {
		this.container = container;
	}

	@Override
	public String toString () {
		return "MonitorEntity{" +
				"callback=" + callback +
				", container=" + container +
				", id=" + id +
				'}';
	}
}
